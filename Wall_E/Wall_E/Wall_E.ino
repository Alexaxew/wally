/* Constantes */

#define DEBUG true
// Motores
const int motor1A = 6;
const int motor1B = 9;
const int motor2A = 10;
const int motor2B = 5;
// Ultrasonidos
const int EchoPin = 4;
const int TriggerPin = 3;
// Sensores de luz LDR
const int sensor0 = A1;
const int sensor1 = A2;
const int sensor2 = A3;
const int sensor3 = A4;
const int sensor4 = A5;

const int botoncin=12;

/* Variables globales */
// Valores de disparo (media entre el valor en blanco y en negro)
// Indican que han encontrado la línea
int linea[5];
int lecturaActual[5];

void setup() { // configuro el puerto serie y los pines de entrada salida. configuracion o cosas que solo quiero que funcionen una sola vez. 
  boolean button= true;
  pinMode (botoncin, INPUT_PULLUP);
  Serial.begin(115200);
  pinMode(motor1A, OUTPUT);
  pinMode(motor1B, OUTPUT);
  pinMode(motor2A, OUTPUT);
  pinMode(motor2B, OUTPUT);
  pinMode(TriggerPin, OUTPUT);
  pinMode(EchoPin, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);

 
  calibrar();
  digitalWrite(LED_BUILTIN, HIGH);
  while (button) {    
   button= digitalRead (botoncin);
  }
  delay(5000); // Espera 5" para empezar
}


void loop() { // ejecuta una y otra y otra y otra vez lo mismo hasta " running out of battery".
  int cm = ping(TriggerPin, EchoPin);
  leer();

  if (hayLineaBlanca()) {
    motores (0, 0, 50 , 255);
    // Serial.println("linea detectada");
  } else if (cm < 20) {
    //motores izqdelante derdelante izqatras deratras
    motores (255, 255, 0, 0);
    //Serial.println("enemigo detectado");
  } else {
    motores (255, 30, 0, 0);
    // Serial.println("buscando enemigo");
  }
  delay(100);
}
