/* Constantes */

#define DEBUG false
// Motores
const int motor1A = 6;
const int motor1B = 9;
const int motor2A = 10;
const int motor2B = 5;

// Sensores de luz LDR
const int sensor0 = A1;
const int sensor1 = A2;
const int sensor2 = A3;
const int sensor3 = A4;
const int sensor4 = A5;

const int botoncin=12;

/* Variables globales */
// Valores de disparo (media entre el valor en blanco y en negro)
// Indican que han encontrado la línea
int linea[5];
int lecturaActual[5];

void setup() { // configuro el puerto serie y los pines de entrada salida. configuracion o cosas que solo quiero que funcionen una sola vez.
   boolean button= true;
   pinMode (botoncin, INPUT_PULLUP);
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
delay(500);
  Serial.begin(115200);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);
  pinMode(motor1A, OUTPUT);
  pinMode(motor1B, OUTPUT);
  pinMode(motor2A, OUTPUT);
  pinMode(motor2B, OUTPUT);

  
  calibrar();
  digitalWrite(LED_BUILTIN, HIGH);
   while (button){
    button= digitalRead (botoncin);
  }
  delay(2000); // Espera 5" para empezar. MIRAR ESTOOOO!
}


void loop() { // ejecuta una y otra y otra y otra vez lo mismo hasta " running out of battery".
  leer();

  if (hayLineaNegracentro()) {
    motores (255,255,0,0);
    // Serial.println("gira a la derecha");
  } else if (hayLineaNegraderecha()) {
   motores (255, 0, 0 ,255);  
    // Serial.println("a toda ostia");
  } else if  (hayLineaNegraizquierda()){
    motores (0, 255, 255 , 0);
    // Serial.println("gira a la izquierda");
  } else {
    motores (200,200,0,0);
  }
  //delay(1000); // ejecuta cada x segundos.
}
