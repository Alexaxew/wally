#define NUM_SENSORES 5


void leer() {
  for (int i = 0; i < NUM_SENSORES; i++) {
    lecturaActual[i] = (analogRead (i + 1) + analogRead (i + 1)) / 2; //lectualActual es la variable que yo almaceno en la base.
    if (DEBUG) {
//      Serial.print("Lectura Actual ");
//      Serial.print(i);
//      Serial.print(" ");
//      Serial.println(lecturaActual[i]);
    }
  }
}



bool hayLineaNegraizquierda() { // la linea es negra. 
 if (linea[4]> lecturaActual[4]) {
   return true;
} else {return false;}
}

bool hayLineaNegracentro() { // la linea es negra. 
 if (linea[2]> lecturaActual[2]) {
   return true;
 }else {return false;}
}

 bool hayLineaNegraderecha() { // la linea es negra. 
 if (linea[0]> lecturaActual[0]) {
   return true;
 }
 else {return false;}
}
void calibrar() {
  unsigned long fin = 5000L; // Para contar el tiempo, 5 segundos
  unsigned int lineaMax[NUM_SENSORES]; // Array de enteros, guarda los valores máximos
  unsigned int lineaMin[NUM_SENSORES]; // Array de enteros, guarda los valores mínimos
  int i = 0; // Para los bucles
  unsigned int temp = 0; // Variable temporal para analogRead()

  for (i = 0; i < NUM_SENSORES; i++) {
    lineaMax[i] = 0;
    lineaMin[i] = 1024;
  }
  // Durante 5" buscamos el maximo y minimo de cada sensor
  fin += millis(); // Tiempo actual (millis) + 5 segundos (fin)// me devuelve desde que se enciende el arduino cuanto tiempo pasa
  while (millis() <= fin) { // Mientras no hayan pasado los 5 segundos
    for (i = 1; i <= NUM_SENSORES; i++) {
      temp = analogRead(i); // Leemos el sensor "i"
      if (temp > lineaMax[i - 1]) lineaMax[i - 1] = temp; // Si es mayor que lo que había, se sobreescribe
      if (temp < lineaMin[i - 1]) lineaMin[i - 1] = temp; // Si es menor que lo que había, se sobreescribe
    }
  }

  for (i = 0; i < NUM_SENSORES; i++) {
    linea[i] = (lineaMax[i] + lineaMin[i]) / 2;
    if (DEBUG) {
      Serial.print("para sensor ");
      Serial.print(i);
      Serial.print(" max ");
      Serial.print(lineaMax[i]);
      Serial.print(" min ");
      Serial.print(lineaMin[i]);
      Serial.print(" dif ");
      Serial.print(lineaMax[i] - lineaMin[i]);
      Serial.print(" - ");
      Serial.println(linea[i]);
    }
  }

  // Calculamos la media
  // *Como lineax son variables globales, ya les queda guardado el resultado
}
